class Team():

    """
    name: the name of the team
    nb_solved: the number of problems solved by the team
    time: the total time (inlcuding penalities)
    results: an array with one result per problem in order
    """
    def __init__(self, name, results):
        #self.name = ''.join([i if ord(i) < 128 else '?' for i in name])
        self.name = name
        self.results = results
        self.time = 0
        self.nb_solved = 0
        for res in results:
            if res.solved:
                self.time += res.time + 20 * (res.nb_tries - 1)
                self.nb_solved += 1
    
    def project(self, timestap):
        results = [ ]
        for res in self.results:
            if res.solved and res.time <= timestap:
                results.append(res)
            else:
                results.append(Result(False, 0, 0))
        return Team(self.name, results)

    def tied(self, other):
        return self.nb_solved == other.nb_solved and self.time == other.time

    def __lt__(self, other):
        if self.nb_solved > other.nb_solved:
            return True
        elif self.nb_solved < other.nb_solved:
            return False
        if self.time < other.time:
            return True
        elif self.time > other.time:
            return False
        return self.name < other.name

    def __str__(self):
        return '{0} {1} {2}'.format(self.name, self.nb_solved, self.time)

class Result():

    """
    solved: whether the problem was solved
    nb_tries: total number of tries (including AC)
    time: time ac submission (does not include penality)
    """
    def __init__(self, solved, nb_tries, time):
        self.solved = solved
        self.nb_tries = nb_tries
        self.time = time

    def __str__(self):
        if self.solved:
            return '{0} / {1} ({2})'.format(1, self.nb_tries, self.time)
        else:
            return '{0}'.format(self.nb_tries)