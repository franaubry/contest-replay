from HTMLParser import HTMLParser
from bs4 import BeautifulSoup
import urllib
from team import *

def parse(url):
    try:
        f = urllib.urlopen(url)
        html = f.read()
    except:
        return None, None, None
    parsers = [domjudege_old_parse, domjudge_parse, kattis_parse, mooshak_parse]
    for parser in parsers:
        try:
            contest_name, nb_problems, teams = parser(html)
            return contest_name, nb_problems, teams
        except:
            pass
    return None, None, None

def parse_file(filename):
    f = open(filename, 'r')
    lines = [ line.strip() for line in f.readlines() ]
    html = ''
    for line in lines:
        html += line + '\n'
    parsers = [domjudege_old_parse, domjudge_parse, kattis_parse, mooshak_parse]
    parsers = [mipt_parse]    
    for parser in parsers:
        contest_name, nb_problems, teams = parser(html)
        return contest_name, nb_problems, teams
    return None, None, None

def convert(s):
  return ''.join(char for char in s if ord(char) < 128)

def parse_csv(filename):
  f = open(filename)
  lines = [ convert(line).strip() for line in f.readlines() ]
  nb_problems = len(lines[0].split('\t')) - 1
  teams = [ ]
  for i in range(1, len(lines)):
    data = lines[i].split('\t')
    teamname = data[0]
    results = [ ]
    for j in range(1, 1 + nb_problems):
      time = int(data[j])
      if time == -1:
        results.append(Result(False, 0, 0))
      else:
        results.append(Result(True, 1, time))
    teams.append(Team(teamname, results))
    print(results)
  return filename.split('.')[0], nb_problems, teams

def mooshak_parse(html):
    soup = BeautifulSoup(html, "lxml")
    title = soup.findAll('title')
    contest_name = title[0].text
    scoreboard = soup.find(attrs={'class':'Listing'})
    soup = BeautifulSoup(str(scoreboard), "lxml")
    cols = soup.findAll('th')
    nb_problems = 0
    for col in cols:
        soup = BeautifulSoup(str(col), "lxml")
        font = soup.findAll('font')
        if len(font) == 1:
            nb_problems += 1
    soup = BeautifulSoup(str(scoreboard), "lxml")
    rows = soup.findAll('tr')
    teams = [ ]
    for i in range(2, len(rows)):
        soup = BeautifulSoup(str(rows[i]), "lxml")
        data = soup.findAll('td')
        teamname = data[1]
        soup = BeautifulSoup(str(teamname), "lxml")
        teamname = soup.findAll('font')[0].text.strip()
        results = [ ]
        for i in range(2, 2 + nb_problems):
            solved = data[i].text.split(' ')
            if len(solved) == 2:
                nb_tries = int(solved[1][1:-1])
                if solved[0] == '------':
                    results.append(Result(False, nb_tries, 0))
                else:
                    time_data = solved[0].split(':')
                    time = int(time_data[0]) * 60 + int(time_data[1])
                    results.append(Result(True, nb_tries, time))
            else:
                results.append(Result(False, 0, 0))
        teams.append(Team(teamname, results))
    return contest_name, nb_problems, teams    

    
    
"""
tested:
nwerc2012
nwerc2013
"""
def domjudege_old_parse(html):
    soup = BeautifulSoup(html, "lxml")
    contest_name = soup.find('h1').text.strip()
    scoreboard = soup.find(attrs={'class':'scoreboard'})
    soup = BeautifulSoup(str(scoreboard), "lxml")
    rows = soup.findAll('tr')
    soup = BeautifulSoup(str(rows[0]), "lxml")
    cols = soup.findAll('th')
    nb_problems = len(cols) - 3
    # get the team results
    teams = [ ]
    for i in range(1, len(rows) - 1):
        soup = BeautifulSoup(str(rows[i]), "lxml")
        data = soup.findAll('td')
        soup = BeautifulSoup(str(data[2]), "lxml")
        teamname = soup.find(text=True)
        results = [ ]
        # read the problem results
        for j in range(len(data) - nb_problems, len(data)):
            score = data[j].text.strip()
            if '+' in score:
                tmp = score.split(' ')
                nb_tries = int(tmp[0])
                time = int(tmp[1][1:])
                results.append(Result(True, nb_tries, time))
            else:
                results.append(Result(False, int(score), 0))
        teams.append(Team(teamname, results))
    return contest_name, nb_problems, teams    

"""
tested:
nwerc2016
"""
def domjudge_parse(html):
    soup = BeautifulSoup(html, "lxml")
    contest_name = soup.find('h1').text.strip()
    scoreboard = soup.find(attrs={'class':'scoreboard'})
    soup = BeautifulSoup(str(scoreboard), "lxml")
    rows = soup.findAll('tr')
    soup = BeautifulSoup(str(rows[0]), "lxml")
    cols = soup.findAll('th')
    nb_problems = len(cols) - 3
    # get the team results
    teams = [ ]
    for i in range(1, len(rows) - 1):
        soup = BeautifulSoup(str(rows[i]), "lxml")
        data = soup.findAll('td')
        soup = BeautifulSoup(str(data[2]), "lxml")
        teamname = soup.find(text=True)
        results = [ ]
        # read the problem results
        for j in range(len(data) - nb_problems, len(data)):
            score = data[j].text.strip()
            if '/' in score:
                tmp = score.split('/')
                nb_tries = int(tmp[0])
                time = int(tmp[1])
                results.append(Result(True, nb_tries, time))
            else:
                results.append(Result(False, int(score), 0))
        teams.append(Team(teamname, results))
    return contest_name, nb_problems, teams

"""
tested:
nwerc2014
nwerc2015
some random kattis contests
"""
def kattis_parse(html):=
    print('kattis')
    soup = BeautifulSoup(html, "lxml")
    title = soup.findAll('title')
    contest_name = title[0].text
    print(contest_name)
    scoreboard = soup.find(id='standings')
    soup = BeautifulSoup(str(scoreboard), "lxml")
    rows = soup.findAll('tr')
    # get the number of problems
    soup = BeautifulSoup(str(rows[0]), "lxml")
    problems = soup.findAll('th', attrs={'class':'problemcolheader'})
    if len(problems) == 0:
        problems = soup.findAll('th', attrs={'class':'problemcolheader-standings'})
    nb_problems = len(problems)
    # get the team results
    teams = [ ]
    for i in range(1, len(rows)):
        row = rows[i]
        soup = BeautifulSoup(str(row), "lxml")
        data = soup.findAll('td')
        if len(data) < 2 + nb_problems:
            # we reached the end of the scoreboard
            break
        rank = int(data[0].text.strip())
        teamname = data[1].text.strip()
        # read the problem results
        results = [ ]
        for j in range(len(data) - nb_problems, len(data)):
            soup = BeautifulSoup(str(data[j]), "lxml")
            res = soup.findAll()
            if len(res) == 3:
                # no sub
                results.append(Result(False, 0, 0))
            elif len(res) == 4:
                # failed sub
                nb_tries = int(res[0].text.split('--')[0].strip())
                results.append(Result(False, nb_tries, 0))
            elif len(res) == 5:
                # correct sub
                tmp = res[0].text.strip().split('\n')
                nb_tries = int(tmp[0])
                time = int(tmp[1])
                results.append(Result(True, nb_tries, time))
            else:
                assert False
        teams.append(Team(teamname, results))
    return contest_name, nb_problems, teams

def mipt_parse(html):
  soup = BeautifulSoup(html, "lxml")
  title = soup.find('title')
  contest_name = title.text
  scoreboard = soup.find('table', { 'class' : 'standings' })
  soup = BeautifulSoup(str(scoreboard).replace('<br/>', '#'), "lxml")
  cols = soup.findAll('th')
  nb_problems = len(cols) - 7
  print(nb_problems)
  rows = soup.findAll('tr')
  teams = [ ]
  for i in range(1, len(rows) - 3):
    row = rows[i]
    soup = BeautifulSoup(str(row), "lxml")
    data = soup.findAll('td')
    teamname = data[1].text
    results = [ ]
    for j in range(nb_problems):
      result = data[2 + j].text.split('#')
      if len(result) == 1:
        results.append(Result(False, 0, 0))
      else:
        tmp = result[1].split(':')
        time = 60 * int(tmp[0]) + int(tmp[1])
        if len(result[0]) == 1:
          results.append(Result(True, 1, time))
        else:
          if result[0][0] == '-':
            nb_tries = int(result[0][1:])
            results.append(Result(False, nb_tries, time))   
          else:
            nb_tries = int(result[0][1:])
            results.append(Result(True, nb_tries + 1, time))        
    teams.append(Team(teamname, results))
  return contest_name, nb_problems, teams

    

def american_parse(html):
    soup = BeautifulSoup(html, "lxml")
    title = soup.findAll('title')
    contest_name = title[0].text
    rows = soup.findAll('tr')
    # get the number of problems
    soup = BeautifulSoup(str(rows[0]), "lxml")
    cols = soup.findAll('th')
    if len(cols) == 0:
        cols = soup.findAll('th')
    nb_problems = len(cols) - 5
    # get the team results
    teams = [ ]
    for i in range(2, len(rows) - 1):
        row = rows[i]
        soup = BeautifulSoup(str(row), "lxml")
        data = soup.findAll('td')
        # TODO: end?
        rank = int(data[0].text.strip())
        teamname = data[1].text.strip()
        # read the problem results
        results = [ ]
        for j in range(nb_problems):
            text = data[j + 4].text.strip()
            tmp = text.split('/')
            nb_subs = int(tmp[0])
            time_ac = 0
            solved = False
            if tmp[1][-1] != '-':
                solved = True
                time_ac = int(tmp[1])
            #print(Result(solved, nb_subs, time_ac))
            print(time_ac)
            results.append(Result(solved, nb_subs, time_ac))
        teams.append(Team(teamname, results))
    return contest_name, nb_problems, teams
