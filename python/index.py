#!/usr/bin/env python

import web
import time
from parsers import *

from web import form

ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
TOTAL_TIME = 300
SPEED = 1
DELTA_TIME = 0

urls = ('/', 'index', '/replay', 'replay')
app = web.application(urls, globals())

if web.config.get('_session') is None:
    store = web.session.DiskStore('sessions')
    session = web.session.Session(app,store,initializer={'count': 0})
    web.config._session = session
else:
    session = web.config._session
render = web.template.render('templates/')


def get_form():
  return form.Form( 
    form.Textbox('contest', class_="form-control"), 
    form.Button('submit', html="Replay contest", class_="btn btn-default")
  )

def format_time(time):
    h = int(time // 60)
    print(h)
    m = time % 60
    print(m)
    hstr = str(h)
    if h < 10:
        hstr = '0' + hstr
    mstr = str(m)
    if m < 10:
        mstr = '0' + mstr
    return hstr + ':' + mstr

def make_html_scoreboard(title, nb_problems, teams, timestamp):
  # project the teams to the current timestamp
  teams_cur = [ team.project(timestamp) for team in teams ]
  teams_cur.sort()
  # build the table header
  html = '<h3>' + title + '</h3>'
  if timestamp == TOTAL_TIME:
    html += '<div>Time left: {0}</div>\n'.format('contest ended')
  else:
    html += '<div>Time left: {0}</div>\n'.format(format_time(TOTAL_TIME - timestamp))
    percent = (100 * timestamp) // TOTAL_TIME
    html += '<div class="progress">\n'
    html += '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{0}" aria-valuemin="0" aria-valuemax="100" style="width: {0}%">\n'.format(percent)
    html += '<span class="sr-only">{0}% Complete</span>'.format(percent)
    html += '{0}%\n'.format(percent)
    html += '</div>\n'
    html += '</div>\n'

  html += '<table class="table">\n'
  html += '<thead>\n'
  html += '<tr>\n'
  html += '<th>Rank</th>\n'
  html += '<th class="name">Team</th>\n'
  html += '<th class="problems">Number of problems solved</th>\n'
  html += '<th class="score">Total time</th>\n'
  for i in range(nb_problems):
    html += '<th class="score text-center">{0}</th>\n'.format(ALPHABET[i])
  html += '</tr>\n'
  html += '</thead>\n'
 
  # build the table body
  html += '<tbody>\n'
  
  rank = 1
  for i in range(len(teams_cur)):
    # build the row for team i
    if i > 0 and not teams_cur[i - 1].tied(teams_cur[i]):
      rank += 1
    html += '<tr>\n'
    html += '<td>{0}</td>\n'.format(rank)
    html += u'<td>' + unicode(teams_cur[i].name) + u'</td>\n'
    html += '<td>{0}</td>\n'.format(teams_cur[i].nb_solved)
    html += '<td>{0}</td>\n'.format(teams_cur[i].time)
    for j in range(nb_problems):
      res = teams_cur[i].results[j]
      if res.solved:
        html += '<td class="score text-center success">{0}/{1}</td>\n'.format(res.nb_tries, res.time)
      elif res.nb_tries > 0:
        html += '<td class="score text-center danger">{0}</td>\n'.format(res.nb_tries)
      else:
        html += '<td class="score text-center"></td>\n'
    html += '</tr>\n'
  html += '</tbody>\n'
  html += '</table>\n'
  return html

class index: 

  def GET(self):
     myform = get_form()
     return render.index(myform, None)

  def POST(self): 
    myform = get_form()
    if myform.validates():
      # get the data from the form
      contest_url = myform.contest.value
      print('getting data from {0}'.format(contest_url))
      #contest_name, nb_problems, teams = parse(contest_url)
      contest_name, nb_problems, teams = parse_file('./score.html')
      #contest_name, nb_problems, teams = parse_csv('./score.csv')
      if contest_name == None:
        return render.index(myform, 'could not parse the contest')
      html = ''
      html += make_html_scoreboard(contest_name, nb_problems, teams, min(0, TOTAL_TIME))
      session.url = contest_url
      session.contest_name = contest_name
      session.nb_problems = nb_problems
      session.teams = teams
      session.start_time = time.time()
      raise web.seeother('/replay')
    else:
      return render.index(myform, 'invalid  form')

class replay:

  def GET(self):
    if session.url == None:
      return render.index(myform, 'please insert the contest url')
    html = ''
    enlapsed = int(SPEED * ((time.time() - session.start_time) / 60 + DELTA_TIME))
    html += make_html_scoreboard(session.contest_name, session.nb_problems, session.teams, min(enlapsed, TOTAL_TIME))
    return render.replay(html) 

if __name__=="__main__":
  app.run()
  print('running')
