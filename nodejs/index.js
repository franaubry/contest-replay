var express = require('express');
var parser = require('./lib/parsers.js')
var app = express();
var afterLoad = require('after-load');
var handlebars = require('express-handlebars').create({ defaultLayout:'main'});

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
app.use(express.static(__dirname + '/public'));
app.set('port', process.env.PORT || 3000);
app.use(require('body-parser').urlencoded( { extended: true } ));

app.use(function(req, res, next) {
    res.locals.showTests = app.get('env') !== 'production' && req.query.test === '1';
    next();
});

app.post('/process', function(req, res) {
    url = req.body.contestUrl;
    afterLoad(url, function(html) {
        parser.parseHtml(html);
        res.redirect(303, '/');
    });
});

app.get('/', function(req, res) {
    res.render('home');
});

// custom 404 page
app.use(function(req, res, next) {
    res.status(404);
    res.render('404');
});

// custom 500 page
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

app.listen(app.get('port'), function() {
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminale.');
});