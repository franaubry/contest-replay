var JSSoup = require('jssoup').default;
var DOMParser = require('xmldom').DOMParser;

const KATTIS_NB_PRE_ROWS = 4;

exports.parseHtml = function(html) {
    parseKattis(html);
};

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function parseKattis(html) {
    console.log('parsing kattis');
    console.log(html);
    /*
    html = `
    <table>
    <thead>
        <tr>
            <th>h1</th>
            <th>h2</th>
            <th>h3</th>
        </tr>
    </thead>
        <tr>
            <td>11<br>--</td>
            <td>12<br>--</td>
            <td>13<br>--</td>
        </tr>
        <tr>
            <td>21<br>--</td>
            <td>22<br>--</td>
            <td>23<br>--</td>
        </tr>
        <tr>
            <td>31<br>--</td>
            <td>32<br>--</td>
            <td></td>
        </tr>
    </table>
    `
    */
    html = replaceAll(html, '<br>', ' ');
    var soup = new JSSoup(html);
    var cur = soup.nextElement;
    var col = 0;
    var row = 0;
    var thCount = 0;
    var nbCols = undefined;
    var rowData = undefined;
    var teamData = [];
    var foundStandings = false;
    while(cur != undefined) {
        if(!foundStandings) {
            if(cur.name == 'table' && cur.attrs != undefined && cur.attrs.id == 'standings') {
                foundStandings = true;
            }
        } else {
            if(cur.name == 'tr') {
                if(col > 0 && nbCols == undefined) {
                    nbCols = col;
                }
                col = 0;
            }
            if(cur.name == 'td') {
                if(col == 0) {
                    if(rowData != undefined) {
                        if(rowData.length < nbCols) {
                            break;
                        }
                        //console.log(rowData);
                        teamData.push(rowData);
                    }
                    rowData = [];
                }
                var text = replaceAll(cur.text.trim(), ' ', '');
                rowData.push(text);
                col += 1;
            }
            if(cur.name == 'th') {
                col += 1;
                thCount += 1;
            }
        }
        cur = cur.nextElement;
    }
    var nbTeams = teamData.length;
    console.log('number of teams: ' + nbTeams);
    var nbProblems = nbCols - KATTIS_NB_PRE_ROWS;
    console.log('number of teams: ' + nbProblems);
    var teams = [];
    var shift = teamData[0].length - nbCols;
    for(var i = 0; i < teamData.length; i++) {
        var team = { }
        team['rank'] = teamData[i][0];
        team['name'] = teamData[i][1];
        team['nbSolved'] = teamData[i][2 + shift];
        team['totalTime'] = teamData[i][3 + shift];
        problems = { }
        for(var j = 0; j < nbProblems; j++) {
            var k = j + 4 + shift;
            if(teamData[i][k].length == 0) {
                problems[j] = {'nbTries': 0, 'acTime': -1};
            } else {
                var data = teamData[i][k].split('\n');
                var nbTries = parseInt(data[0]);
                var acTime = -1;
                if(data[1] != '--') {
                    acTime = parseInt(data[1]);
                }
                problems[j] = {'nbTries': nbTries, 'acTime': acTime};
            }
        }
        team['problems'] = problems;
        teams.push(team);
    }
    for(var i = 0; i < nbTeams; i++) {
        console.log(teams[i]);
    }
    /*
    cur = soup.nextElement;
    
    // look fo the standings
    while(cur != undefined) {
        if(cur.attrs != undefined && cur.attrs['id'] == 'standings') {
            console.log('found standings');
            break;
        }
        cur = cur.nextElement;
    }
    // count the number of problems
    nb_problems = 0;
    while(cur != undefined) {
        if(cur.name == 'th') {
            nb_problems += 1;
        }
        cur = cur.nextElement;
    }
    console.log(nb_problems);
    */
}